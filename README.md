# Fissa

Real-time audio visualisation thing for parties.

## Installation

- Run `npm install`
- Wait a year
- Done

## Usage

- Run `npm start`
- A page will magically appear in your default browser
- 🎉

### Keyboard shortcuts

Key | Effect
-|-
q | Loop through textures
w | Loop through blending modes
e | Loop through colours
a | Loop through scenes
c | Toggle interface controls

## Adding/changing images/videos

1. In `public/assets/scripts/app.js`, find the `load()` function. This is where resources are loaded.
2. You will need to load the resource first. `this._resourceManager` has `.loadImage(name, url)` and `.loadVideo(name, url)` methods to load images and videos respectively. Add or modify one of such lines.
3. Find the line starting with `this._resourceManager.set('textureImageMix'`. All images and videos need to be converted to a texture. That's what happens here.
4. Add or modify an object. If the texture is a texture, `needsUpdate` need to be set to `false`. If the texture is a video, make sure `needsUpdate` is `true` and the `image` property is set, referring to the resource added in step 2.

### Example texture objects

Image

```js
{
    // Image textures don't need updates
    needsUpdate: false,

    // Create a texture for imageCustom
    texture: this._regl.texture({
        data: this._resourceManager.get('imageCustom'),
        flipY: true,
    })
}
```

Video

```js
{
    // Refer to the original video resource
    image: this._resourceManager.get('videoCustom'),

    // Video textures needs to be updated every frame
    needsUpdate: true,

    // Create a texture for videoCustom
    texture: this._regl.texture({
        data: this._resourceManager.get('videoCustom'),
        flipY: true,
    })
}
```

## Roadmap

### Short-term

- Define presets
  - Easy preset
  - Fissa™ preset
- Map more Pioneer DDJ-RB inputs

### Long-term

- Refactor entire code
- Move keyboard and Pioneer DDJ-RB input mapping to an adapter
- Feed content by user input (rather than in the bloody repository)
- Web-based control panel
