precision highp float;

uniform vec3 uColorPeak;
uniform vec3 uColorValley;
varying float vAmplitude;

void main() {
	gl_FragColor = vec4(mix(uColorValley, uColorPeak, vAmplitude), 1);
}
