precision highp float;

uniform vec2 uCircles[CIRCLE_COUNT];
uniform vec2 uResolution;
uniform float uCircleRadius;

uniform vec3 uColor;

void main() {
    vec2 aspect = vec2(uResolution.x / uResolution.y, 1.0);
    vec2 uv = gl_FragCoord.xy / uResolution;
    vec2 position = (uv * 2.0 - 1.0) * vec2(1.0, -1.0) * aspect;
    float value = 0.0;

    for (int i = 0; i < CIRCLE_COUNT; i++) {
        vec2 circle = uCircles[i];
        vec2 difference = position - circle;

        value += (
					(uCircleRadius * uCircleRadius) /
					(difference.x * difference.x + difference.y * difference.y) /
					CIRCLE_COUNT.0
				);
    }

		float b = 1.0 - step(value, 0.075);

		gl_FragColor = vec4(b * uColor, b);
}
