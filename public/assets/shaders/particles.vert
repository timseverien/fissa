precision mediump float;

attribute float angle;
attribute vec2 position;
attribute float radius;
uniform float uIntensity;

void main() {
	vec2 offset = vec2(
		uIntensity * radius * cos(angle),
		uIntensity * radius * sin(angle)
	);

	gl_PointSize = 4.0;
	gl_Position = vec4(position + offset, 0, 1);
}
