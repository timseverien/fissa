// https://js1k.com/2016-elemental/details/2552

precision highp float;

uniform vec2 uMutation;
uniform vec2 uPhase;
uniform vec2 uResolution;
uniform float uTime;
uniform float uScale;
uniform vec3 colorBackground;
uniform vec3 colorForeground;
uniform vec3 colorForegroundShade;

const float MATH_E = 2.71828182846;
const float MATH_PI = 3.14159265359;

float t = 5e-3;

float gaussian(float mu, float sigmaSquared, float x) {
	float a = (x - mu);

	return MATH_E * -((a * a) / (2.0 * sigmaSquared));
}

vec3 blend(vec3 color, float t) {
float a = clamp(2.0 * t, 0.0, 1.0);
    float c = clamp(2.0 * (1.0 - t), 0.0, 1.0);
    float b = 1.0 - a - c;
    return b * color + c; // = a*vec3(0,0,0)+b*color+c*vec3(1,1,1)
}

void main() {
	for(float i = 0.0; i < 64.0; i++) {
		vec3 p = vec3((2.0 * gl_FragCoord.xy - uResolution) / uResolution.yy / uScale, t - 1.0);
		vec3 b = vec3(0.707, 0.707, 0);
		float a = MATH_PI * sin(2.0 * MATH_PI * uTime);

		p.xz *= mat2(cos(a), -sin(a), sin(a), cos(a));

		for(float i = 0.0; i < 20.0; i++) {
			a = uMutation.x + uPhase.x;
			p.xz *= mat2(cos(a), -sin(a), sin(a), cos(a));

			a = uMutation.y + uPhase.y;
			p.xy *= mat2(cos(a), -sin(a), sin(a), cos(a));

			p -= min(0., dot(p, b)) * b * 2.0;
			b = b.zxx;

			p -= min(0.0, dot(p, b)) * b * 2.0;
			b = b.zxz;

			p -= min(0.0, dot(p, b)) * b * 2.0;
			b = b.xxy;

			p = p * 1.5 - 0.25;
		}

		t += length(p) / 3325.0;

		if(length(p) / 3325.0 < 5e-3 || t > 2.0) {
			float tColor = i / 20.0;

			if (t >= 2.0) {
				discard;
				break;
			}

			vec3 color = mix(colorForegroundShade, colorForeground, tColor);
			gl_FragColor = vec4(color, 1.0);

			break;
		}
	}
}
