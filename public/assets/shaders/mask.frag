precision highp float;

uniform sampler2D textureMask;
uniform sampler2D textureSource;
uniform vec2 uMaskResolution;
uniform vec2 uSourceResolution;

void main() {
	vec2 uvMask = gl_FragCoord.xy / uMaskResolution.xy;
	vec2 uvSource = gl_FragCoord.xy / uSourceResolution.xy;

	gl_FragColor = (
		texture2D(textureMask, uvMask).r *
		texture2D(textureSource, uvSource)
	);
}
