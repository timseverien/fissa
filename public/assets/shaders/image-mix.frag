precision highp float;

uniform sampler2D textureImage;
uniform sampler2D textureSource;
uniform float uSourceMix;
uniform vec2 uResolution;
uniform int uMode;

const float EPSILON = 1.0 / 128.0;
const float EPSILON_INVERSE = 1.0 - EPSILON;

void main() {
	vec2 uv = gl_FragCoord.xy / uResolution.xy;
	vec4 a = texture2D(textureSource, uv);
	vec4 b = texture2D(textureImage, uv);

	if (uMode == 0) gl_FragColor = uSourceMix * a;
	if (uMode == 1) gl_FragColor = uSourceMix * b;
	if (uMode == 2) gl_FragColor = uSourceMix * a + b;
	if (uMode == 3) gl_FragColor = uSourceMix * a * b;
	if (uMode == 4) gl_FragColor = uSourceMix * a / b;
	if (uMode == 5) gl_FragColor = min(uSourceMix * a, b);
	if (uMode == 6) gl_FragColor = max(uSourceMix * a, b);
}
