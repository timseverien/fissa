precision highp float;

uniform sampler2D textureSource;
uniform vec2 uResolution;

void main() {
	vec2 uv = gl_FragCoord.xy / uResolution.xy;

	gl_FragColor = texture2D(textureSource, uv);
}
