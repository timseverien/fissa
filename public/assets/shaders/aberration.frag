precision highp float;

uniform sampler2D textureSource;
uniform vec2 uResolution;
uniform float uAberration;

void main() {
	vec2 uv = gl_FragCoord.xy / uResolution.xy;
	vec4 source = texture2D(textureSource, uv);
  vec2 pixel = 1.0 / uResolution.xy;
	vec2 offset = uAberration * vec2(pixel.x, 0.0);

	float r = texture2D(textureSource, uv + offset).r;
	float b = texture2D(textureSource, uv - offset).b;

	gl_FragColor = vec4(r, source.g, b, source.a);
}
