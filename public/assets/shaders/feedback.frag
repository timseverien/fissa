precision highp float;

uniform sampler2D textureSource;
uniform sampler2D textureSourceFeedback;
uniform vec2 uResolutionBuffer;
uniform vec2 uResolutionSource;
uniform float uMix;

void main() {
	vec2 uvBuffer = gl_FragCoord.xy / uResolutionBuffer.xy;
	vec2 uvSource = gl_FragCoord.xy / uResolutionSource.xy;

	gl_FragColor = mix(texture2D(textureSource, uvSource), texture2D(textureSourceFeedback, uvBuffer), uMix);
}
