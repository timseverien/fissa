import GeometryFactory from './factories/geometry.js';
import ResourceManager from './managers/resource.js';
import createAberrationPass from './passes/aberration.js';
import createDistortPass from './passes/distort.js';
import createFeedbackPass from './passes/feedback.js';
import createFractalPass from './passes/fractal.js';
import createImageMixPass from './passes/image-mix.js';
import createMaskPass from './passes/mask.js';
import createPassthroughPass from './passes/passthrough.js';
import createTerrainPass from './passes/terrain.js';
import createShapesPass from './passes/shapes.js';
import MathUtils from './utils/math.js';

export default class App {
	constructor(canvas, regl, colorPalette) {
		this._activeColorsetIndex = 0;
		this._activeColorIndex = 0;
		this._activeImageMixTexture = 0;
		this._activeSceneIndex = 0;
		this._canvas = canvas;
		this._colorsets = colorPalette;
		this._frameBuffers = new Map();
		this._imageMixMode = 0;
		this._imageMixModeCount = 7;
		this._postProcessing = null;
		this._regl = regl;
		this._renderToScreen = null;
		this._renderToScreenBuffer = null;
		this._resourceManager = new ResourceManager();
		this._scenes = [];

		this._renderPerspective = this._regl({
			context: {
				model: [
					1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1,
				],

				projection: (context) => mat4.perspective([],
					Math.PI / 4,
					context.viewportWidth / context.viewportHeight,
					0.01,
					1000.0),

				view: (context, props) => mat4.lookAt([], props.camera, [0, 0, 0], [0, 1, 0]),
				eye: regl.prop('camera')
			},

			uniforms: {
				model: regl.context('model'),
				projection: regl.context('projection'),
				view: regl.context('view'),
			}
		});
	}

	dispose() {
		this._scenes = null;
		this._regl.destroy();
	}

	initialize() {
		const plane = GeometryFactory.plane(this._regl);

		this._renderToScreen = createPassthroughPass(this._regl, {
			geometry: plane,
			shader: this._resourceManager.get('shaderPassthrough'),
		});

		this.initializeFrameBuffers();
		this.initializePostProcessing(plane);
		this.initializeScenes(plane);
	}

	initializeFrameBuffers() {
		const frameBufferSize = MathUtils.getNextPowerOfTwo(this._maxSize);
		const bufferPostProcessingOptions = {
			depth: false,
			depthStencil: false,
			height: frameBufferSize,
			max: 'linear',
			min: 'linear',
			width: frameBufferSize,
			stencil: false,
			wrap: ['repeat', 'repeat'],
		};

		this._frameBuffers.set('screen', this._regl.framebuffer({
			depth: false,
			depthStencil: false,
			height: this._canvas.height,
			width: this._canvas.width,
			stencil: false,
		}));

		this._frameBuffers.set('feedbackInternalBufferA', this._regl.framebuffer(bufferPostProcessingOptions));
		this._frameBuffers.set('feedbackInternalBufferB', this._regl.framebuffer(bufferPostProcessingOptions));
		this._frameBuffers.set('postProcessingA', this._regl.framebuffer(bufferPostProcessingOptions));
		this._frameBuffers.set('postProcessingB', this._regl.framebuffer(bufferPostProcessingOptions));

		this._renderToScreenBuffer = this._regl({
			framebuffer: this._frameBuffers.get('screen')
		});
	}

	initializePostProcessing(geometry) {
		const renderAberration = createAberrationPass(this._regl, {
			aberration: (context, props) => 0.05 * this._canvas.width * props.inputs.chromaticAberration,
			geometry,
			shader: this._resourceManager.get('shaderAberration'),
		});

		const renderFeedback = createFeedbackPass(this._regl, {
			geometry,
			mix: (context, props) => MathUtils.lerp(0.5, 0.9, props.volume),
			shader: this._resourceManager.get('shaderFeedback'),
			shaderPassthrough: this._resourceManager.get('shaderPassthrough'),
			renderPass: createDistortPass(this._regl, {
				distort: (context, props) => this._minSize / 100 * props.volume * props.volume * props.volume,
				time: (context, props) => props.time,
				geometry,
				shader: this._resourceManager.get('shaderDistort'),
			}),
			renderPassFrameBuffers: [
				this._frameBuffers.get('feedbackInternalBufferA'),
				this._frameBuffers.get('feedbackInternalBufferB'),
			],
		});

		const renderImageMix = createImageMixPass(this._regl, {
			geometry,
			shader: this._resourceManager.get('shaderImageMix'),
			image: () => this._resourceManager.get('textureImageMix')[this._activeImageMixTexture].texture,
			sourceMix: (context, props) => props.inputs.imageMixMix,
			mode: () => this._imageMixMode,
		});

		const renderMask = createMaskPass(this._regl, {
			geometry,
			shader: this._resourceManager.get('shaderMask'),
			textureMask: this._resourceManager.get('textureMask'),
		});

		this._postProcessing = (properties) => {
			renderImageMix(Object.assign({}, properties, {
				source: this._frameBuffers.get('screen'),
				renderTarget: this._frameBuffers.get('postProcessingA'),
			}));

			renderFeedback(Object.assign({}, properties, {
				source: this._frameBuffers.get('postProcessingA'),
				renderTarget: this._frameBuffers.get('postProcessingB'),
			}));

			renderAberration(Object.assign({}, properties, {
				source: this._frameBuffers.get('postProcessingB'),
				renderTarget: this._frameBuffers.get('postProcessingA'),
				resolution: [
					this._frameBuffers.get('postProcessingB').width,
					this._frameBuffers.get('postProcessingB').height,
				],
			}));

			renderMask(Object.assign({}, properties, {
				source: this._frameBuffers.get('postProcessingA'),
				renderTarget: this._frameBuffers.get('screen'),
				resolutionMask: [
					this._frameBuffers.get('screen').width,
					this._frameBuffers.get('screen').height,
				],
				resolutionSource: [
					this._frameBuffers.get('postProcessingB').width,
					this._frameBuffers.get('postProcessingB').height,
				],
			}));
		};
	}

	initializeScenes(geometryPlane) {
		const geometryTerrain = GeometryFactory.lineMesh(this._regl, 4, 4, 31, 31);
		const renderTerrain = createTerrainPass(this._regl, {
			amplitude: (context, props) => MathUtils.lerp(0.5, 2, props.volume),
			colorPeak: () => this._activeColorset[1].rgbFloat,
			colorValley: () => this._activeColorset[0].rgbFloat,
			geometry: geometryTerrain,
			shader: this._resourceManager.get('shaderTerrain'),
			time: (context, props) => props.time + 2 * props.timeSound,
		});

		this._scenes.push(createFractalPass(this._regl, {
			geometry: geometryPlane,
			shader: this._resourceManager.get('shaderFractal'),
			phase: (context, props) => [
				0.005 * props.inputs.modificationA,
				0.005 * props.inputs.modificationB
			],
			colorBackground: [0, 0, 0],
			colorForeground: () => this._activeColorset[1].rgbFloat,
			colorForegroundShade: () => this._activeColorset[0].rgbFloat,
		}));

		this._scenes.push(createShapesPass(this._regl, {
			circleCount: 16,
			circleRadius: (context, props) => (1 / 8) + props.inputs.modificationA / 1024,
			geometry: geometryPlane,
			shader: this._resourceManager.get('shaderShapes'),
			color: () => this._activeColorset[this._activeColorIndex].rgbFloat,
		}));

		this._scenes.push(properties => this._renderPerspective(properties, () => renderTerrain(properties)));
	}

	async load() {
		await Promise.all([
			this._resourceManager.loadImage('imageMask', 'assets/images/mask.png'),
			this._resourceManager.loadImage('imageMix1', 'assets/images/hoi.jpg'),
			this._resourceManager.loadImage('imageMix2', 'assets/images/noise.png'),
			// this._resourceManager.loadVideo('imageMix3', 'assets/images/test.mp4'),
			// this._resourceManager.loadVideo('imageMix4', 'assets/images/wwe.mp4'),
			// this._resourceManager.loadVideo('imageMix5', 'assets/videos/fuuu.mp4'),
			this._resourceManager.loadShader('shaderAberration', 'assets/shaders/aberration.frag', 'assets/shaders/passthrough.vert'),
			this._resourceManager.loadShader('shaderDistort', 'assets/shaders/distort.frag', 'assets/shaders/passthrough.vert'),
			this._resourceManager.loadShader('shaderFeedback', 'assets/shaders/feedback.frag', 'assets/shaders/passthrough.vert'),
			this._resourceManager.loadShader('shaderFractal', 'assets/shaders/fractal.frag', 'assets/shaders/passthrough.vert'),
			this._resourceManager.loadShader('shaderImageMix', 'assets/shaders/image-mix.frag', 'assets/shaders/passthrough.vert'),
			this._resourceManager.loadShader('shaderMask', 'assets/shaders/mask.frag', 'assets/shaders/passthrough.vert'),
			this._resourceManager.loadShader('shaderShapes', 'assets/shaders/shapes.frag', 'assets/shaders/passthrough.vert'),
			this._resourceManager.loadShader('shaderTerrain', 'assets/shaders/terrain.frag', 'assets/shaders/terrain.vert'),
			this._resourceManager.loadShader('shaderPassthrough', 'assets/shaders/passthrough.frag', 'assets/shaders/passthrough.vert'),
		]);

		// Nieuwe afbeelding toevoegen: kopieër array item (`{...}`) en vervang naam (`this._resourceManager.get('NAAM')`)
		this._resourceManager.set('textureImageMix', [
			// IMAGES
			{
				needsUpdate: false,
				texture: this._regl.texture({
					data: this._resourceManager.get('imageMix1'),
					flipY: true,
				})
			},
			{
				needsUpdate: false,
				texture: this._regl.texture({
					data: this._resourceManager.get('imageMix2'),
					flipY: true,
				}),
			},

			// VIDEOS
			// {
			// 	image: this._resourceManager.get('imageMix3'),
			// 	needsUpdate: true,
			// 	texture: this._regl.texture({
			// 		data: this._resourceManager.get('imageMix3'),
			// 		flipY: true,
			// 	}),
			// },
			// {
			// 	image: this._resourceManager.get('imageMix4'),
			// 	needsUpdate: true,
			// 	texture: this._regl.texture({
			// 		data: this._resourceManager.get('imageMix4'),
			// 		flipY: true,
			// 	}),
			// },
			// {
			// 	image: this._resourceManager.get('imageMix5'),
			// 	needsUpdate: true,
			// 	texture: this._regl.texture({
			// 		data: this._resourceManager.get('imageMix5'),
			// 		flipY: true,
			// 	}),
			// },
		]);

		this._resourceManager.set('textureMask', this._regl.texture(this._resourceManager.get('imageMask')));
	}

	nextColorset() {
		this._activeColorsetIndex = (1 + this._activeColorsetIndex) % this._colorsets.length;
		this._activeColorIndex = (1 + this._activeColorIndex) % 2;
	}

	nextImage() {
		const images = this._resourceManager.get('textureImageMix');

		this._activeImageMixTexture = (1 + this._activeImageMixTexture) % images.length;
	}

	nextImageMode() {
		this._imageMixMode = (1 + this._imageMixMode) % this._imageMixModeCount;
	}

	nextScene() {
		const clear = () => this._regl.clear({
			color: [0, 0, 0, 0],
			depth: 1,
		});

		this._frameBuffers.forEach(framebuffer => this._regl({ framebuffer })(clear));

		this._activeSceneIndex = (1 + this._activeSceneIndex) % this._scenes.length;
	}

	render(options) {
		const properties = Object.assign({}, options, {
			renderTarget: this._frameBuffers.get('screen'),
			resolution: [this._canvas.width, this._canvas.height],
		});

		this._resourceManager.get('textureImageMix').forEach((textureData, index) => {
			if (!textureData.needsUpdate) return;

			return textureData.texture.subimage(textureData.image);
		});

		this._renderToScreenBuffer(() => {
			this._regl.clear({ color: [0, 0, 0, 0] });
			this._scenes[this._activeSceneIndex](properties);
		});

		if (typeof this._postProcessing === 'function') {
			this._postProcessing(properties);
		}

		this._renderToScreen(Object.assign({}, properties, {
			renderTarget: null,
			source: this._frameBuffers.get('screen'),
		}));

		this._scenes[this._activeSceneIndex](properties);
	}

	setSize(width, height) {
		this._canvas.height = height;
		this._canvas.width = width;

		Array.from(this._frameBuffers.entries()).forEach(({
			1: frameBuffer
		}) => {
			frameBuffer.resize(width, height);
		});
	}

	get _activeColorset() {
		return this._colorsets[this._activeColorsetIndex];
	}

	get _maxSize() {
		return Math.max(this._canvas.height, this._canvas.width);
	}

	get _minSize() {
		return Math.min(this._canvas.height, this._canvas.width);
	}
};
