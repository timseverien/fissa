export default {
	getNextPowerOfTwo(n) {
		return Math.pow(2, Math.ceil(Math.log(n) / Math.log(2)));
	},

	lerp(a, b, t) {
		return a + t * (b - a);
	},
};
