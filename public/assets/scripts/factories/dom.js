export default {
    inputNumber(value = 0) {
        const element = document.createElement('input');

        element.type = 'number';
        element.value = value;

        return element;
    },

    inputRange(min = 0, max = 1, step = 0.001, value = 0) {
        const element = document.createElement('input');

        element.type = 'range';
        element.step = step;
        element.min = min;
        element.max = max;
        element.value = value;

        return element;
    },
};