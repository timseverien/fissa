export default {
	create(setup, teardown, render) {
		return {
			render,
			setup,
			teardown,
		};
	}
};
