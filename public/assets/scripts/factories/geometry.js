import MathUtils from '../utils/math.js';

const getIndexByCoordinates = (x, y, columnCount) => x + y * columnCount;
const getCoordinatesByIndex = (index, columnCount) => [
	index % columnCount,
	Math.floor(index / columnCount),
];

export default {
	particles(regl, particleCount) {
		const vertices = new Array(particleCount).fill([0, 0]);

		return {
			count: particleCount,
			elements: null,
			vertices,
		};
	},

	plane(regl) {
		const vertices = [
			[-1,  1],
			[ 1,  1],
			[-1, -1],
			[ 1, -1],
		];

		return {
			elements: regl.elements({
				data: [
					[0, 1, 2],
					[1, 3, 2],
				],
				primitive: 'triangles',
			}),
			vertices,
		};
	},

	mesh(regl, scaleX = 1, scaleY = 1, segmentsX = 1, segmentsY = 1) {
		const scaleXHalf = 0.5 * scaleX;
		const scaleYHalf = 0.5 * scaleY;

		const columnCount = 1 + segmentsX;
		const rowCount = 1 + segmentsY;
		const vertexCount = columnCount * rowCount;
		const vertices = new Array(vertexCount).fill().map((_, i) => getCoordinatesByIndex(i, columnCount));

		const elements = vertices.reduce((indices, [x, y], index) => {
			if (x < segmentsX && y < segmentsY) {
				indices.push([ index, index + 1, index + columnCount ]);
				indices.push([ index + 1, index + 1 + columnCount, index + columnCount ]);
			}

			return indices;
		}, []);

		return {
			elements: regl.elements({
				data: elements,
				primitive: 'triangles',
			}),
			vertices: vertices.map(([x, y]) => [
				MathUtils.lerp(-scaleXHalf, scaleXHalf, x / segmentsX),
				0,
				MathUtils.lerp(-scaleYHalf, scaleYHalf, y / segmentsY),
			]),
		};
	},

	lineMesh(regl, scaleX = 1, scaleY = 1, segmentsX = 1, segmentsY = 1) {
		const scaleXHalf = 0.5 * scaleX;
		const scaleYHalf = 0.5 * scaleY;

		const columnCount = 1 + segmentsX;
		const rowCount = 1 + segmentsY;
		const vertexCount = columnCount * rowCount;
		const vertices = new Array(vertexCount).fill().map((_, i) => getCoordinatesByIndex(i, columnCount));

		const elements = vertices.reduce((indices, [x, y], index) => {
			if (x < segmentsX) {
				indices.push([ index, index + 1 ]);
			}

			if (y < segmentsY) {
				indices.push([ index, index + columnCount ]);
			}

			if (x < segmentsX && y < segmentsY) {
				indices.push([ index + 1, index + columnCount ]);
			}

			return indices;
		}, []);

		return {
			elements: regl.elements({
				data: elements,
				primitive: 'lines',
			}),
			vertices: vertices.map(([x, y]) => [
				MathUtils.lerp(-scaleXHalf, scaleXHalf, x / segmentsX),
				0,
				MathUtils.lerp(-scaleYHalf, scaleYHalf, y / segmentsY),
			]),
		};
	},
};
