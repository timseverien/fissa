export default (regl, {
	geometry,
	image,
	mode,
	shader,
	sourceMix,
}) => regl({
	elements: geometry.elements,
	frag: shader.fragment,
	vert: shader.vertex,
	framebuffer: regl.prop('renderTarget'),

	attributes: {
		position: geometry.vertices,
	},

	uniforms: {
		textureImage: image,
		textureSource: regl.prop('source'),
		uSourceMix: sourceMix,
		uMode: mode,
		uResolution: regl.prop('resolution'),
	},
});
