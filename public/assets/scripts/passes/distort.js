export default (regl, {
	distort,
	geometry,
	shader,
	time,
}) => regl({
	elements: geometry.elements,
	frag: shader.fragment,
	vert: shader.vertex,
	framebuffer: regl.prop('renderTarget'),

	attributes: {
		position: geometry.vertices,
	},

	uniforms: {
		textureSource: regl.prop('source'),
		uDistort: distort,
		uResolution: regl.prop('resolution'),
		uTime: time,
	},
});
