export default (regl, {
	aberration,
	geometry,
	shader,
}) => regl({
	elements: geometry.elements,
	frag: shader.fragment,
	vert: shader.vertex,
	framebuffer: regl.prop('renderTarget'),

	attributes: {
		position: geometry.vertices,
	},

	uniforms: {
		textureSource: regl.prop('source'),
		uAberration: aberration,
		uResolution: regl.prop('resolution'),
	},
});
