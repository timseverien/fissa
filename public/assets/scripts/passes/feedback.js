import createPassthroughPass from '../passes/passthrough.js';

export default (regl, {
	geometry,
	mix,
	renderPass,
	renderPassFrameBuffers,
	shader,
	shaderPassthrough,
}) => {
	const resolutionFrameBuffer = [
		renderPassFrameBuffers[0].width,
		renderPassFrameBuffers[0].height,
	];

	const renderFeedback = regl({
		elements: geometry.elements,
		frag: shader.fragment,
		vert: shader.vertex,
		framebuffer: regl.prop('renderTarget'),

		attributes: {
			position: geometry.vertices,
		},

		uniforms: {
			textureSource: regl.prop('source'),
			textureSourceFeedback: regl.prop('sourceFeedback'),
			uMix: mix,
			uResolutionBuffer: resolutionFrameBuffer,
			uResolutionSource: regl.prop('resolutionSource'),
		},
	});

	const renderPassthrough = createPassthroughPass(regl, {
		geometry,
		shader: shaderPassthrough,
	});

	return (properties) => {
		renderFeedback(Object.assign({}, properties, {
			source: properties.source,
			sourceFeedback: renderPassFrameBuffers[0],
			renderTarget: renderPassFrameBuffers[1],
			resolutionSource: [properties.source.width, properties.source.height],
		}));

		renderPass(Object.assign({}, properties, {
			source: renderPassFrameBuffers[1],
			renderTarget: renderPassFrameBuffers[0],
			resolution: resolutionFrameBuffer,
		}));

		renderPassthrough(Object.assign({}, properties, {
			source: renderPassFrameBuffers[0],
			renderTarget: properties.renderTarget,
			resolution: resolutionFrameBuffer,
		}));
	};
};
