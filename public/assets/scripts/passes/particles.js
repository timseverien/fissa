export default (regl, {
	geometry,
	shader,
}) => regl({
	count: geometry.count,
	primitive: 'points',
	frag: shader.fragment,
	vert: shader.vertex,
	framebuffer: regl.prop('renderTarget'),

	attributes: {
		angle: new Array(geometry.count).fill().map(() => 2 * Math.PI * Math.random()),
		radius: new Array(geometry.count).fill().map(() => Math.random()),
		position: geometry.vertices,
	},

	uniforms: {
		uResolution: regl.prop('resolution'),
		uIntensity: regl.prop('volume'),
	},
});;
