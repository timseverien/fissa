import MathUtils from '../utils/math.js';

export default (regl, {
	colorBackground,
	colorForeground,
	colorForegroundShade,
	phase,
	geometry,
	shader,
}) => regl({
	elements: geometry.elements,
	frag: shader.fragment,
	vert: shader.vertex,
	framebuffer: regl.prop('renderTarget'),

	attributes: {
		position: geometry.vertices,
	},

	uniforms: {
		colorBackground,
		colorForeground,
		colorForegroundShade,
		uPhase: phase,
		uResolution: regl.prop('resolution'),

		uScale(context, props) {
			const t = 0.5 * (Math.sin(2 * Math.PI * props.timeSound / 30) + 1);

			return MathUtils.lerp(1, 1.5, t);
		},

		uMutation(context, props) {
			return [
				Math.PI * props.timeSound / 120,
				2 * Math.PI * props.time / 120,
			];
		},

		uTime(context, props) {
			return Math.sin(props.time / 240);
		},
	},
});
