export default (regl, {
	amplitude,
	colorPeak,
	colorValley,
	geometry,
	shader,
	time,
}) => regl({
	elements: geometry.elements,
	frag: shader.fragment,
	vert: shader.vertex,
    framebuffer: regl.prop('renderTarget'),
	lineWidth: 1,

	attributes: {
		position: geometry.vertices,
	},

	uniforms: {
		uAmplitude: amplitude,
		uTime: time,
		uColorPeak: colorPeak,
		uColorValley: colorValley,
	},
});
