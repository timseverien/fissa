export default (regl, {
	geometry,
	shader,
	textureMask,
}) => regl({
	elements: geometry.elements,
	frag: shader.fragment,
	vert: shader.vertex,
	framebuffer: regl.prop('renderTarget'),

	attributes: {
		position: geometry.vertices,
	},

	uniforms: {
		textureMask: textureMask,
		textureSource: regl.prop('source'),
		uMaskResolution: regl.prop('resolutionMask'),
		uSourceResolution: regl.prop('resolutionSource'),
	},
});
