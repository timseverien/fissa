const getPositionFactory = (seed) => (context, props) => {
	const a = Math.sin(6523 * seed + 12) + Math.sin(342 * seed) * Math.sin(869 * seed);
	const b = Math.sin(867 * seed + 984) + Math.sin(684 * seed) * Math.sin(3489 * seed);

	return [
		0.75 * Math.sin(props.time * a),
		0.75 * Math.sin(props.time * b),
	];
};

const getCircles = circleCount => new Array(circleCount).fill()
    .map((_, index) => getPositionFactory(index / circleCount))
    .reduce((uniforms, value, index) => {
        uniforms[`uCircles[${index}]`] = value;

        return uniforms;
    }, {});

export default (regl, {
	color,
	circleCount,
	circleRadius,
	geometry,
	shader,
}) => regl({
	elements: geometry.elements,
	frag: shader.fragment.replace(/CIRCLE_COUNT/g, circleCount),
	vert: shader.vertex,
	framebuffer: regl.prop('renderTarget'),

	attributes: {
		position: geometry.vertices,
	},

	uniforms: Object.assign({}, getCircles(circleCount), {
		uCircleRadius: circleRadius,
		uColor: color,
		uResolution: regl.prop('resolution'),
	}),
});
