export default class MicrophoneSensor {
  constructor() {
    this._context = new AudioContext();
    this._source = null;

    this._analyser = this._context.createAnalyser();
    this._analyser.fftSize = MicrophoneSensor.FFT_SIZE;
    this._analyser.smoothingTimeConstant = 0;

    this.data = new Uint8Array(this._analyser.frequencyBinCount);
  }

  dispose() {
    this.disposeSource();

    this._context.close();
    this._context = null;
    this.data = null;
  }

  disposeSource() {
    if (!this.isSourceAvailable) return;

    this._source.disconnect(this._analyser);
    this._source = null;
  }

  async start() {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });

    this._source = this._context.createMediaStreamSource(stream);
    this._source.connect(this._analyser);
    this._source.addEventListener('ended', () => this.disposeSource());
  }

  update() {
    this._analyser.getByteFrequencyData(this.data);
  }

  get isSourceAvailable() {
    return !!this._source;
  }

  get dataVolume() {
    const frequencyBinCount = Math.floor(0.01 * this.data.length);

    return this.data
      .slice(0, frequencyBinCount)
      .reduce((sum, value) => sum + value / 255, 0) / frequencyBinCount;
  }

  static get FFT_SIZE() {
    return Math.pow(2, 10);
  }
}
