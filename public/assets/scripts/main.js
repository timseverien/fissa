/*
WERKTTITEL 2.0 huisstijl

0xFF5DD4
0x6FD0FB
*/

import DomFactory from './factories/dom.js';
import Color from './models/color.js';
import MicrophoneSensor from './sensors/microphone.js';
import App from './app.js';

const entryValueInputToFloat = ([key, input]) => [key, Number.parseFloat(input.value)];
const entriesToObject = entries => entries.reduce((obj, [key, value]) => Object.assign({}, obj, { [key]: value }), {});

const inputControls = {
	chromaticAberration: DomFactory.inputRange(),
	colorLoopThreshold: DomFactory.inputRange(),
	imageMixMix: DomFactory.inputRange(),
	volumeMultiplier: DomFactory.inputRange(),
	modificationA: DomFactory.inputNumber(),
	modificationB: DomFactory.inputNumber(),
};

const interfaceControls = document.createElement('div');
interfaceControls.classList.add('controls');
interfaceControls.hidden = true;
document.body.appendChild(interfaceControls);

Object.entries(inputControls).forEach(([name, input]) => {
	const div = document.createElement('div');
	const label = document.createElement('div');
	label.innerText = name;

	div.appendChild(label);
	div.appendChild(input);
	interfaceControls.appendChild(div);
});


// Defaults
inputControls.imageMixMix.value = 1;

const colorsets = [
	// Primary to secondary
	[new Color(0xFFFFFF), new Color(0xFFFFFF)],
	[new Color(0xFF0000), new Color(0xFFFF00)],
	[new Color(0xFF00BB), new Color(0x00B1FF)],
	[new Color(0x00B1FF), new Color(0xFF00BB)],
	[new Color(0x0000FF), new Color(0x00FFFF)],
	[new Color(0xFFFF00), new Color(0xFF0088)],

	// Black messes up blobs
	// [new Color(0x000000), new Color(0xFF00BB)],
	// [new Color(0x000000), new Color(0x00B1FF)],
];

const canvas = document.getElementById('canvas');
const regl = createREGL(canvas);

const microphoneSensor = new MicrophoneSensor();
microphoneSensor.start();

const app = new App(canvas, regl, colorsets);
app.setSize(window.innerWidth, window.innerHeight);
// app.setSize(512, 256);

let tick;

const colorsetChangeDelay = 1 / 4;
let previousColorsetChange = 0;

(async () => {
	await app.load();
	app.initialize();

	const timeStart = performance.now() / 1000;
	let timeSound = 0;

	tick = regl.frame(() => {
		const inputs = entriesToObject(Object.entries(inputControls).map(entryValueInputToFloat));
		const time = performance.now() / 1000 - timeStart;
		let volume = 0;

		if (microphoneSensor && microphoneSensor.isSourceAvailable) {
			microphoneSensor.update();

			if (Number.isFinite(microphoneSensor.dataVolume)) {
				volume = 2 * inputs.volumeMultiplier * microphoneSensor.dataVolume;
				timeSound += volume / 60;
			}
		}

		if (previousColorsetChange + colorsetChangeDelay < time && 0.5 * volume > inputs.colorLoopThreshold * inputs.volumeMultiplier) {
			previousColorsetChange = time;
			app.nextColorset();
		}

		app.render({
			camera: [0, 0.25, -1],
			inputs,
			time,
			timeSound,
			volume,
		});
	});
})();

const keyMapping = {
	a: () => {
		app.nextScene();
		inputControls.modificationA.value = 0;
		inputControls.modificationB.value = 0;
	},
	c: () => interfaceControls.hidden = !interfaceControls.hidden,
	e: () => app.nextColorset(),
	q: () => app.nextImage(),
	w: () => app.nextImageMode(),
};

const midiMapping = {
	'176-4': data => inputControls.colorLoopThreshold.value = data[2] / 127,
	'177-4': data => inputControls.chromaticAberration.value = data[2] / 127,
	'182-8': data => inputControls.volumeMultiplier.value = data[2] / 127,
	'182-13': data => inputControls.imageMixMix.value = data[2] / 127,
	'151-0': (data) => {
		if (data[2] !== 127) return;
		app.nextImage();
	},
	'151-1': (data) => {
		if (data[2] !== 127) return;
		app.nextImageMode();
	},
	'151-4': (data) => {
		if (data[2] !== 127) return;
		app.nextScene();
		inputControls.modificationA.value = 0;
		inputControls.modificationB.value = 0;
	},
	'151-5': (data) => {
		if (data[2] !== 127) return;
		app.nextColorset();
	},
	'176-34': data => inputControls.modificationA.value = Number.parseFloat(inputControls.modificationA.value) + (data[2] === 65 ? 1 : -1),
	'177-34': data => inputControls.modificationB.value = Number.parseFloat(inputControls.modificationB.value) + (data[2] === 65 ? 1 : -1),
};

const handleMidiInput = (event) => {
	console.log(event.data); // TODO: Remove

	const midiKey = `${event.data[0]}-${event.data[1]}`;

	if (!Object.keys(midiMapping).includes(midiKey)) return;
	midiMapping[midiKey](event.data);
};

navigator.requestMIDIAccess().then(midi => [...midi.inputs.values()].forEach(i => i.addEventListener('midimessage', handleMidiInput)));

window.addEventListener('beforeunload', () => {
	if (tick) tick.cancel();
	app.dispose();
});

window.addEventListener('keydown', (e) => {
	console.log(e.key);

	if (!Object.keys(keyMapping).includes(e.key)) {
		return;
	}

	keyMapping[e.key]();
});
