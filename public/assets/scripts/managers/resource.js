import ImageLoader from '../loaders/image.js';
import ShaderLoader from '../loaders/shader.js';
import VideoLoader from '../loaders/video.js';

export default class ResourceManager {
	constructor() {
		this._resources = new Map();
	}

	get(name) {
		return this._resources.get(name);
	}

	has(name) {
		return this._resources.has(name);
	}

	set(name, resource) {
		this._resources.set(name, resource);

		return this;
	}

	async loadImage(name, image) {
		return this.set(name, await ImageLoader.load(image));
	}

	async loadShader(name, shaderFragment, shaderVertex) {
		return this.set(name, await ShaderLoader.load(shaderFragment, shaderVertex));
	}

	async loadVideo(name, video, options = {}) {
		return this.set(name, await VideoLoader.load(video, options));
	}
};
