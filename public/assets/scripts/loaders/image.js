export default class ImageLoader {
	static async load(source) {
		const image = new Image();

		return new Promise((resolve, reject) => {
			image.addEventListener('error', error => reject(error));
			image.addEventListener('load', () => resolve(image), { once: true });
			image.src = source;
		});
	}
}
