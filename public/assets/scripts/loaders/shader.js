export default class ShaderLoader {
	static async load(shaderPathFragment, shaderPathVertex) {
		const [fragment, vertex] = await Promise.all([
			fetch(shaderPathFragment).then(r => r.text()),
			fetch(shaderPathVertex).then(r => r.text()),
		]);

		return {
			fragment, vertex
		};
	}
}
