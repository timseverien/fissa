export default class VideoLoader {
	static async load(source, options = {}) {
		const settings = Object.assign({}, VideoLoader.OPTIONS_DEFAULT, options);
		const video = document.createElement('video');

		video.autoplay = settings.autoplay;
		video.loop = settings.loop;
		video.muted = settings.muted;

		return new Promise((resolve, reject) => {
			video.addEventListener('error', error => reject(error));
			video.addEventListener('loadeddata', () => {
				if (settings.autoplay) {
					video.play();
				}

				resolve(video);
			}, { once: true });

			video.src = source;
		});
	}

	static get OPTIONS_DEFAULT() {
		return {
			autoplay: true,
			loop: true,
			muted: true,
		}
	}
}
