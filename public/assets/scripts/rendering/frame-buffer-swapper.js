export default class FrameBufferSwapper {
	constructor(regl, options = {}) {
		const height = options.height || 1024;
		const width = options.width || 1024;

		this._activeBufferIndex = 0;
		this._buffers = [
			regl.framebuffer(Object.assign({}, options, {
				color: regl.texture({ height, width }),
			})),
			regl.framebuffer(Object.assign({}, options, {
				color: regl.texture({ height, width }),
			})),
		];
	}

	getSource() {
		return this._buffers[this._activeBufferIndex];
	}

	getTarget() {
		return this._buffers[this._getNextIndex(this._activeBufferIndex)];
	}

	resize(width, height) {
		this._buffers.forEach(buffer => buffer.resize(width, height));

		return this;
	}

	swap() {
		this._activeBufferIndex = this._getNextIndex(this._activeBufferIndex);

		return this;
	}

	_getNextIndex(index) {
		return (index + 1) % this._buffers.length;
	}
}
