export default (
	regl,
	shaderFragment,
	shaderVertex,
) => regl({
	frag: shaderFragment,
	vert: shaderVertex,
});
