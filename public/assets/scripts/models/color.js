const bitmask = 0xFF;

export default class Color {
    constructor(value) {
        this._value = value;
    }

    get rgbUint() {
        return [
            (this._value >> 16) & bitmask,
            (this._value >> 8) & bitmask,
            this._value & bitmask
        ];
    }

    get rgbFloat() {
        return this.rgbUint.map(n => n / 255);
    }
}
